/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   str.h                                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: agrouard <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/12/12 09:21:25 by agrouard          #+#    #+#             */
/*   Updated: 2018/12/12 09:27:19 by agrouard         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef STR_H
# define STR_H

# include "libft.h"

typedef struct	s_str
{
	char		*buf;
	size_t		capacity;
	size_t		len;
}				t_str;

t_str			*str_new(char *buf);
void			str_init(t_str *str, char *buf);
void			str_free(t_str *str);
int				str_insert(t_str *str, size_t index, char c);
int				str_cat(t_str *a, t_str *b);

#endif
