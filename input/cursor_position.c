/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   cursor_position.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: marin <marin@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/12/12 09:21:25 by agrouard          #+#    #+#             */
/*   Updated: 2019/02/03 10:02:33 by marin            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <unistd.h>
#include "input.h"
#include "mapping.h"

int	set_cursor(t_abuf *ab, int x, int y)
{
	char	buf[80];
	char	*temp;

	buf[0] = 0;
	ft_strcat(buf, "\x1b[");
	if ((temp = ft_itoa(y + 1)) == NULL)
		return (-1);
	ft_strcat(buf, temp);
	free(temp);
	ft_strcat(buf, ";");
	if ((temp = ft_itoa(x + 1)) == NULL)
		return (-1);
	ft_strcat(buf, temp);
	free(temp);
	ft_strcat(buf, "H");
	return (ab_append(ab, buf, ft_strlen(buf)));
}

int	get_cursor(int *x, int *y)
{
	char		buf[32];
	unsigned	i;
	char		*temp;

	if (write(STDOUT_FILENO, "\x1b[6n", 4) != 4)
		return (-1);
	i = 0;
	while (i < sizeof(buf) - 1)
	{
		if (read(STDIN_FILENO, buf + i, 1) != 1)
			break ;
		if (buf[i] == 'R')
			break ;
		i++;
	}
	buf[i] = 0;
	if (buf[0] != '\x1b' || buf[1] != '[')
		return (-1);
	if (y)
		*y = ft_atoi(&buf[2]) - 1;
	if ((temp = ft_strchr(&buf[2], ';')) == NULL)
		return (-1);
	if (x)
		*x = ft_atoi(temp + 1) - 1;
	return (0);
}

int	move_cursor(int c)
{
	t_str	*l;

	l = (t_str *)g_in->lines.data[g_in->lines.size - 1];
	if (c == ARROW_LEFT && g_in->cx != 0)
		g_in->cx--;
	else if (c == ARROW_RIGHT && (unsigned)g_in->cx != l->len)
		g_in->cx++;
	else if (c == KEY_HOME || c == CTRL('a'))
		g_in->cx = 0;
	else if (c == KEY_END || c == CTRL('e'))
		g_in->cx = l->len;
	return (2);
}

int del_word(int c)
{
	t_str	*l;
	char	buffer[256];
	int		i;

	l = (t_str *)g_in->lines.data[g_in->lines.size - 1];
	i = 0;
	ft_memset(buffer, 0, 256);
	if (c == CTRL_BACKSPACE)
	{
		while (g_in->cx >= 0 && !ft_isspace(l->buf[g_in->cx]) && i < 256)
		{
			buffer[i++] = l->buf[g_in->cx];
			del_char(KEY_DELETE);
			g_in->cx--;
		}
		ft_strrev(buffer);
		g_in->cx++;
	}
	else
	{
		while (l->buf[g_in->cx] && !ft_isspace(l->buf[g_in->cx]) && i < 256)
		{
			buffer[i++] = l->buf[g_in->cx];
			del_char(KEY_DELETE);
		}
	}
	set_clipboard(buffer, 0, ft_strlen(buffer));
	return (2);
}

int	move_word(int c)
{
	t_str	*l;

	l = (t_str *)g_in->lines.data[g_in->lines.size - 1];
	if (c == CTRL_LEFT)
	{
		while (g_in->cx != 0)
			if (!ft_isspace(l->buf[--g_in->cx]))
				break ;
		while (ft_isalnum(l->buf[g_in->cx]) && g_in->cx != 0)
			g_in->cx--;
	}
	if (c == CTRL_RIGHT)
	{
		while (ft_isspace(l->buf[g_in->cx]) &&
				(unsigned)g_in->cx != l->len)
			g_in->cx++;
		while (ft_isalnum(l->buf[g_in->cx]) &&
				(unsigned)g_in->cx != l->len)
			g_in->cx++;
	}
	return (2);
}

int	move_multiline(int c)
{
	t_str	*l;
	char	*n;

	l = (t_str *)g_in->lines.data[g_in->lines.size - 1];
	if (c == CTRL_UP)
	{
		n = l->buf + g_in->cx;
		if (*n == '\n')
			n--;
		while (*n != '\n' && n > l->buf)
			n--;
		if (n <= l->buf)
			n = l->buf;
		g_in->cx = n - l->buf;
	}
	if (c == CTRL_DOWN)
	{
		n = l->buf + g_in->cx;
		while (*n != '\n' && n < l->buf + l->len)
			n++;
		if (n >= l->buf + l->len)
			n = l->buf + l->len - 1;
		g_in->cx = n + 1 - l->buf;
	}
	return (2);
}

