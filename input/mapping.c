/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   mapping.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: agrouard <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/12/12 09:21:25 by agrouard          #+#    #+#             */
/*   Updated: 2018/12/12 09:27:19 by agrouard         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <unistd.h>
#include "shell/shell.h"
#include "mapping.h"
#include "input.h"

t_str		g_clipboard = {NULL, 0, 0};

int		move_history(int c)
{
	char	*cmd;
	t_str	*str;

	if (c == ARROW_UP)
		c = -1;
	else if (c == ARROW_DOWN)
		c = 1;
	else
	{
		g_in->hist = 0;
		return (0);
	}
	if (!(str = malloc(sizeof(t_str))))
		return (1);
	ft_arrclear(&g_in->lines);
	if ((cmd = fetch_history(&g_in->hist, c)))
		cmd = ft_strdup(cmd);
	str_init(str, cmd);
	ft_arrpush(&g_in->lines, str);
	g_in->cx = str->len;
	return (1);
}

int		discard(int c)
{
	g_in->cx = ((t_str *)g_in->lines.data[g_in->lines.size - 1])->len;
	refresh_screen();
	ft_putchar('\n');
	if (c == CTRL('c'))
	{
		g_in->interrupted = 1;
		ft_arrclear(&g_in->lines);
		g_in->cx = 0;
		g_in->cy = 0;
		g_in->line = 0;
		g_in->curr = 0;
	}
	else if (c == CTRL('d'))
	{
		if (g_in->cx == 0)
			g_sh->run = 0;
		else
			return (1);
	}
	return (0);
}

void		set_clipboard(char *buf, size_t a, size_t b)
{
	if (b - a > g_clipboard.capacity)
	{
		free(g_clipboard.buf);
		g_clipboard.capacity = b - a;
		g_clipboard.buf = ft_strnew(g_clipboard.capacity);
	}
	ft_memmove(g_clipboard.buf, buf + a, b - a);
	g_clipboard.len = b - a;
	g_clipboard.buf[g_clipboard.len] = '\0';
}

int		cut(int c)
{
	t_str *l;

	l = (t_str *)g_in->lines.data[g_in->lines.size - 1];
	if (c == CTRL('u'))
	{
		set_clipboard(l->buf, 0, g_in->cx);
		ft_memmove(l->buf, l->buf + g_in->cx, l->len - g_in->cx + 1);
		l->len -= g_in->cx;
		g_in->cx = 0;
	}
	else if (c == CTRL('k'))
	{
		set_clipboard(l->buf, g_in->cx, l->len);
		l->buf[g_in->cx] = '\0';
		l->len = g_in->cx;
	}
	return (2);
}

int		paste(int c)
{
	t_str	*l;
	char	*temp;

	(void)c;
	l = (t_str *)g_in->lines.data[g_in->lines.size - 1];
	if (g_clipboard.len + l->len > l->capacity)
	{
		l->capacity = g_clipboard.len + l->len;
		temp = ft_strnew(l->capacity);
		ft_memmove(temp, l->buf, g_in->cx);
	}
	else
		temp = l->buf;
	ft_memmove(temp + g_in->cx + g_clipboard.len,
			l->buf + g_in->cx, l->len - g_in->cx);
	ft_memmove(temp + g_in->cx,
			g_clipboard.buf, g_clipboard.len);
	if (temp != l->buf)
		free(l->buf);
	l->buf = temp;
	l->len = g_clipboard.len + l->len;
	l->buf[l->len] = '\0';
	g_in->cx += g_clipboard.len;
	return (2);
}
