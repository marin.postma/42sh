/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   input.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: agrouard <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/12/12 09:21:25 by agrouard          #+#    #+#             */
/*   Updated: 2018/12/12 09:27:19 by agrouard         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <unistd.h>
#include "libft.h"
#include "input.h"
#include "shell/shell.h"

static int	read_next_line(void)
{
	int		ret;
	char	*line;
	t_str	*l;

	if ((ret = get_next_line(0, &line)) == 1)
	{
		if (g_in->interrupted)
		{
			ft_arrclear(&g_in->lines);
			g_in->line = 0;
			g_in->curr = 0;
			ret = 0;
		}
		if ((l = add_line(line)))
			l->buf[l->len] = '\n';
		else
		{
			free(line);
			ret = -1;
		}
	}
	else if (!(g_sh->run = 0) && g_sh->interactive)
		ft_putchar('\n');
	return (ret);
}

static int	read_line(t_input *in, char *prompt)
{
	int	ret;
	t_str	*l;

	init_prompt(in, prompt);
	if (g_sh->interactive == 2)
	{
		add_line(NULL);
		refresh_screen();
		while ((ret = process_keypress()) == 2)
			refresh_screen();
		disable_raw_mode();
	}
	else
	{
		if (g_sh->interactive == 1)
			write(1, in->prompt.buf, in->prompt.len);
		ret = read_next_line();
	}
	if (ret == 1)
	{
		l = (t_str *)in->lines.data[in->lines.size - 1];
		if (l->len && l->buf[l->len - 1] == '\\')
			ret = read_line(in, "> ");
	}
	return (ret);
}

char		peek_char(t_input *in, char *prompt)
{
	t_str	*l;

	if (in->line == in->lines.size)
	{
		if ((in->line || in->curr) && !prompt)
			return (0);
		if (read_line(in, prompt) == 1)
			return (peek_char(in, prompt));
		return (0);
	}
	l = (t_str *)in->lines.data[in->line];
	if (l->buf[in->curr] == '\\' && in->curr == l->len - 1)
		return (((t_str *)in->lines.data[in->line + 1])->buf[0]);
	return (l->buf[in->curr]);
}

char		pop_char(t_input *in, char *prompt)
{
	t_str	*l;
	char	c;

	if ((c = peek_char(in, prompt)))
	{
		l = (t_str *)in->lines.data[in->line];
		if (c != l->buf[in->curr] || ++in->curr > l->len)
		{
			in->line++;
			in->curr = 0;
		}
	}
	return (c);
}

char		*pop_line(t_input *in, char *prompt)
{
	t_str	*l;
	char	*res;

	if (in->line == in->lines.size)
	{
		if (read_line(in, prompt) != 1)
			return (NULL);
	}
	l = (t_str *)in->lines.data[in->line++];
	in->curr = 0;
	if (!(res = ft_strnew(l->len)))
		return (NULL);
	ft_memcpy(res, l->buf, l->len);
	res[l->len] = '\0';
	if (l->len && l->buf[l->len - 1] == '\\')
	{
		res[l->len - 1] = '\0';
		return (ft_strjoinf(res, pop_line(in, NULL)));
	}
	return (res);
}
