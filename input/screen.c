/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   screen.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: agrouard <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/12/12 09:21:25 by agrouard          #+#    #+#             */
/*   Updated: 2018/12/12 09:27:19 by agrouard         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <unistd.h>
#include "input.h"

static void	get_char_coords(int *x, int *y, char *buf, size_t index)
{
	size_t i;

	i = 0;
	*x = g_in->prompt.capacity;
	*y = g_in->cy;
	while (i < index)
	{
		if (buf[i] == '\n')
		{
			*x = 0;
			(*y)++;
		}
		else if (*x == g_in->width - 1)
		{
			*x = 0;
			(*y)++;
		}
		else
			(*x)++;
		i++;
	}
}

static void	update_cursor(t_abuf *ab, t_str *l)
{
	int x;
	int y;
	int lasty;

	if (l->len)
	{
		x = l->len - ((unsigned)g_in->cx != l->len);
		get_char_coords(&x, &lasty, l->buf, x);
		if (lasty >= g_in->height)
			g_in->cy -= (lasty - g_in->height + 1);
		if ((unsigned)g_in->cx == l->len && x == 0)
			ab_append(ab, "\n", 1);
	}
	get_char_coords(&x, &y, l->buf, g_in->cx);
	set_cursor(ab, x, y);
}

int			refresh_screen()
{
	t_abuf	ab;
	t_str	*l;

	ab.b = NULL;
	ab.len = 0;
	l = (t_str *)g_in->lines.data[g_in->lines.size - 1];
	ab_append(&ab, "\x1b[?25l", 6);
	set_cursor(&ab, 0, g_in->cy);
	ab_append(&ab, "\x1b[0J", 4);
	ab_append(&ab, g_in->prompt.buf, g_in->prompt.len);
	ab_append(&ab, l->buf, l->len);
	update_cursor(&ab, l);
	ab_append(&ab, "\x1b[?25h", 6);
	write(STDOUT_FILENO, ab.b, ab.len);
	free(ab.b);
	return (0);
}
