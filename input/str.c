/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   str.c                                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: agrouard <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/12/12 09:21:25 by agrouard          #+#    #+#             */
/*   Updated: 2018/12/12 09:27:19 by agrouard         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "str.h"

t_str	*str_new(char *buf)
{
	t_str	*str;

	if (!(str = malloc(sizeof(t_str))))
		return (NULL);
	if (buf)
		str_init(str, buf);
	else
	{
		str->capacity = 8;
		str->len = 0;
		if (!(str->buf = ft_strnew(str->capacity)))
		{
			free(str);
			return (NULL);
		}
	}
	return (str);
}

void	str_init(t_str *str, char *buf)
{
	str->len = buf ? ft_strlen(buf) : 0;
	str->capacity = str->len;
	str->buf = buf;
}

void	str_free(t_str *str)
{
	free(str->buf);
	free(str);
}

int		str_insert(t_str *str, size_t index, char c)
{
	size_t	capacity;

	if (index > str->len)
		index = str->len;
	if (str->len == str->capacity)
	{
		capacity = str->capacity ? str->capacity * 2 : 8;
		if (!(str->buf = ft_memrealloc(str->buf, str->len, capacity + 1)))
			return (1);
		str->buf[str->len] = '\0';
		str->capacity = capacity;
	}
	ft_memmove(str->buf + index + 1, str->buf + index, str->len + 1 - index);
	str->len++;
	str->buf[index] = c;
	return (0);
}

int		str_cat(t_str *a, t_str *b)
{
	size_t	capacity;

	capacity = a->len + b->len;
	if (capacity > a->capacity)
	{
		if ((a->buf = ft_memrealloc(a->buf, a->len, capacity + 1)) == NULL)
			return (1);
		a->buf[a->len] = '\0';
		a->capacity = capacity;
	}
	if (!a->buf || !b->buf)
		return (0);
	ft_memmove(a->buf + a->len, b->buf, b->len + 1);
	a->len += b->len;
	return (0);
}
