/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   init.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: agrouard <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/12/12 09:21:25 by agrouard          #+#    #+#             */
/*   Updated: 2018/12/12 09:27:19 by agrouard         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <sys/ioctl.h>
#include <unistd.h>
#include "input.h"
#include "shell/shell.h"

t_input	*g_in = NULL;

static size_t	prompt_len(char *prompt)
{
	int count;

	count = 0;
	while (*prompt)
	{
		if (*prompt == '\x1b')
			while (*prompt && *prompt != 'm')
				prompt++;
		else
			count++;
		prompt++;
	}
	return (count);
}

void			init_input(t_input *in)
{
	in->interrupted = 0;
	str_init(&in->prompt_default, get_prompt());
	in->prompt_default.capacity = prompt_len(in->prompt_default.buf);
	in->line = 0;
	in->curr = 0;
	ft_arrinit(&in->lines, 1, str_free);
	g_in = in;
}

void			free_input(t_input *in)
{
	size_t	i;
	t_str	*l;

	if (g_sh->interactive)
	{
		i = 0;
		while (i < in->lines.size)
		{
			l = (t_str *)in->lines.data[i++];
			if (l->len && l->buf[l->len - 1] == '\\')
				l->buf[--l->len] = '\0';
			else if (i < in->lines.size)
				str_insert(l, -1, '\n');
			str_cat(&in->cmd, l);
		}
		if (in->cmd.buf)
		{
			in->cmd.buf[in->cmd.len] = '\0';
			push_history(in->cmd.buf);
		}
	}
	ft_arrdestroy(&in->lines);
	free(in->prompt_default.buf);
	g_in = NULL;
}

int				init_size(t_input *in)
{
	struct winsize ws;

	if (ioctl(STDOUT_FILENO, TIOCGWINSZ, &ws) == -1)
		return (-1);
	in->width = ws.ws_col;
	in->height = ws.ws_row;
	return (0);
}

int				init_prompt(t_input *in, char *prompt)
{
	if (!prompt)
		in->prompt = in->prompt_default;
	else
	{
		str_init(&in->prompt, prompt);
		in->prompt.capacity = prompt_len(in->prompt.buf);
	}
	if (g_sh->interactive == 2)
	{
		enable_raw_mode();
		in->hist = 0;
		get_cursor(&in->cx, &in->cy);
		init_size(in);
		if (in->cx)
		{
			ft_putchar('\n');
			in->cx = 0;
			get_cursor(NULL, &in->cy);
		}
	}
	str_init(&in->cmd, NULL);
	return (0);
}
