/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   raw.c                                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: agrouard <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/12/12 09:21:25 by agrouard          #+#    #+#             */
/*   Updated: 2018/12/12 09:27:19 by agrouard         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <unistd.h>
#include "input.h"
#include "shell/shell.h"

int		enable_raw_mode(void)
{
	struct termios raw;

	raw = g_sh->orig_state;
	raw.c_iflag &= ~(BRKINT | INPCK | ISTRIP | ICRNL | IXON);
	raw.c_cflag |= (CS8);
	raw.c_lflag &= ~(ECHO | IEXTEN | ICANON | ISIG);
	raw.c_cc[VMIN] = 0;
	raw.c_cc[VTIME] = 1;
	if (tcsetattr(STDIN_FILENO, TCSAFLUSH, &raw) == -1)
		return (-1);
	return (0);
}

int		disable_raw_mode(void)
{
	if (tcsetattr(STDIN_FILENO, TCSAFLUSH, &g_sh->orig_state) == -1)
		return (-1);
	return (0);
}

t_str	*add_line(char *line)
{
	t_str	*str;

	if (!(str = str_new(line)))
		return (NULL);
	if (ft_arrpush(&g_in->lines, str))
	{
		str_free(str);
		return (NULL);
	}
	return (str);
}
