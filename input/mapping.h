/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   mapping.h                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: marin <marin@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/12/12 09:21:25 by agrouard          #+#    #+#             */
/*   Updated: 2019/02/03 10:11:47 by marin            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef MAPPING_H
# define MAPPING_H

# include <unistd.h>

typedef int	(*t_keymap)(int);

enum		e_special
{
	KEY_ENTER = 13,
	BACKSPACE = 127,
	ESC_BASE = 256,
	CTRL_BACKSPACE = CTRL('w'),
	CTRL_FWDSPACE = CTRL('f'),
	CTRL_UP = 256 + CTRL('A'),
	CTRL_DOWN,
	CTRL_RIGHT,
	CTRL_LEFT,
	ARROW_UP = 256 + 'A',
	ARROW_DOWN,
	ARROW_RIGHT,
	ARROW_LEFT,
	KEY_HOME = 256 + 'H',
	KEY_END = 256 + 'F',
	KEY_DELETE = 256 + 'Z' + 1,
	MAX_SPECIAL
};

int		move_history(int c);
void	set_clipboard(char *buf, size_t a, size_t b);
int		discard(int c);
int		cut(int c);
int		paste(int c);

#endif
