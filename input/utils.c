/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   utils.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: agrouard <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/12/12 09:57:42 by agrouard          #+#    #+#             */
/*   Updated: 2019/02/03 10:03:42 by marin            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char	*ft_strjoinf(char *a, char *b)
{
	char *res;

	res = ft_strjoin(a, b);
	free(a);
	free(b);
	return (res);
}

void ft_strrev(char *str)
{
    int i;
    char temp;  
    int l;
    
    
    i = 0;
    l = strlen(str);
    while (i != l / 2)
    {
        temp = str[i];
        str[i] = str[l - i - 1];
        str[l - i - 1] = temp;
        i++;
    }
}
