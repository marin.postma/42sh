/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   input.h                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: marin <marin@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/12/12 09:21:25 by agrouard          #+#    #+#             */
/*   Updated: 2019/02/03 10:03:29 by marin            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef INPUT_H
# define INPUT_H

# include <termios.h>
# include "str.h"

typedef struct	s_abuf
{
	char		*b;
	int			len;
}				t_abuf;

typedef struct	s_input
{
	int			cx;
	int			cy;
	int			width;
	int			height;
	int			hist;

	t_str		prompt;
	t_str		prompt_default;

	t_array		lines;
	size_t		line;
	size_t		curr;

	t_str		cmd;
	int			interrupted;
}				t_input;

extern t_input	*g_in;

void			init_input(t_input *in);
void			free_input(t_input *in);

char			peek_char(t_input *in, char *prompt);
char			pop_char(t_input *in, char *prompt);
char			*pop_line(t_input *in, char *prompt);

int				init_prompt(t_input *in, char *prompt);
int				enable_raw_mode(void);
int				disable_raw_mode(void);

int				ab_append(t_abuf *ab, const char *s, size_t len);

int		set_cursor(t_abuf *ab, int cols, int rows);
int		get_cursor(int *x, int *y);

int		move_multiline(int c);
int		move_cursor(int c);
int		move_word(int c);
void	ft_strrev(char *str);
int		del_word(int c);
int		del_char(int c);

int		refresh_screen();
void	insert_char(int c);
t_str	*add_line(char *line);

char	*ft_strjoinf(char *a, char *b);

int		process_keypress(void);

#endif
