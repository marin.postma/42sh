/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   utils.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: agrouard <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/12/12 10:40:57 by agrouard          #+#    #+#             */
/*   Updated: 2019/02/03 09:42:52 by marin            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "shell/shell.h"

int		err(char *message, int ret)
{
	ft_putstr_fd(SH_NAME": ", 2);
	ft_putendl_fd(message, 2);
	return (ret);
}

char	*ft_strcjoinf(char *a, char c, char *b)
{
	char *res;

	res = ft_strcjoin(a, c, b);
	free(a);
	free(b);
	return (res);
}

char	*redir_str(t_redir *redir)
{
	static char *chars[] = {"<", ">", "<<", ">>", "<&", ">&", "<>", "<<-"};
	char		*str;
	int			index;

	if (redir->type == '<' || redir->type == '>')
		index = (redir->type == '>');
	else
		index = redir->type - TOK_DLESS + 2;
	str = ft_strcjoinf(ft_itoa(redir->redirected), ' ',
			ft_strdup(chars[index]));
	if (redir->dest.type == REDIR_FD)
		str = ft_strcjoinf(str, ' ', ft_itoa(redir->dest.val.fd));
	else if (redir->dest.type == REDIR_FILE)
		str = ft_strcjoinf(str, ' ', ft_strdup(redir->dest.val.file));
	else if (redir->dest.type == REDIR_CLOSE)
		str = ft_strcjoinf(str, ' ', ft_strdup("-"));
	return (str);
}

char	*proc_str(t_array *words, t_array *redirs, int fg)
{
	size_t	i;
	char	*str;
	t_token	*t;

	i = 1;
	t = words->data[0];
	str = ft_strdup(t->val.str);
	while (i < words->size)
	{
		t = words->data[i++];
		str = ft_strcjoinf(str, ' ', ft_strdup(t->val.str));
	}
	i = 0;
	while (i < redirs->size)
		str = ft_strcjoinf(str, ' ', redir_str(redirs->data[i++]));
	if (!fg)
		str = ft_strcjoinf(str, ' ', ft_strdup("&"));
	return (str);
}

