/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   heredoc.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: agrouard <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/12/12 09:21:25 by agrouard          #+#    #+#             */
/*   Updated: 2018/12/12 09:27:19 by agrouard         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "redir.h"
#include "parser.h"
#include <fcntl.h>

# include <unistd.h>

void		free_redir(t_redir *redir)
{
	if (!redir)
		return ;
	if (redir->dest.type == REDIR_DOC)
		free(redir->dest.val.doc.buf);
	free(redir);
}

static int	append_heredoc(t_redir *redir, t_str *input)
{
	char	*oldb;
	int		oldl;

	if (redir->type == TOK_DLESS)
		return (str_cat(&redir->dest.val.doc, input));
	oldb = input->buf;
	oldl = input->len;
	while (*(input->buf)++ == '\t')
		input->len--;
	input->buf--;
	str_cat(&redir->dest.val.doc, input);
	input->buf = oldb;
	input->len = oldl;
	return (0);
}

int			parse_heredoc(t_redir *redir)
{
	t_str	input;
	char	*line;
	char	*end;

	end = redir->dest.val.file;
	str_init(&redir->dest.val.doc, NULL);
	while ((line = pop_line(g_in, "heredoc> ")))
	{
		if (ft_strequ(line, end))
		{
			free(line);
			return (0);
		}
		str_init(&input, ft_strdup(line));
		free(line);
		str_insert(&input, -1, '\n');
		append_heredoc(redir, &input);
		free(input.buf);
	}
	if (g_in->interrupted)
		free(redir->dest.val.doc.buf);
	return (g_in->interrupted);
}
