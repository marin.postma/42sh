/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   redir.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: agrouard <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/12/12 09:57:42 by agrouard          #+#    #+#             */
/*   Updated: 2018/12/12 13:33:57 by agrouard         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "redir.h"
#include "parser.h"
#include <fcntl.h>

static t_redir	*new_redir(void)
{
	t_redir	*redir;

	if (!(redir = malloc(sizeof(t_redir))))
		return (NULL);
	redir->flags = 0;
	redir->redirected = -1;
	redir->dest.type = REDIR_FD;
	redir->dest.val.fd = -1;
	return (redir);
}

static int		get_redir_flags(int type)
{
	if (type == '>')
		return (O_WRONLY | O_CREAT | O_TRUNC);
	if (type == TOK_DGREAT)
		return (O_WRONLY | O_CREAT | O_APPEND);
	if (type == '<')
		return (O_RDONLY);
	if (type == TOK_LESSGREAT)
		return (O_RDWR | O_CREAT);
	if (type == TOK_LESSAND)
		return (O_RDONLY);
	if (type == TOK_GREATAND)
		return (O_WRONLY);
	return (0);
}

static int		do_parse_more(t_redir *redir, t_token *tok, t_token *tok2)
{
	redir->dest.val.file = tok2->val.str;
	redir->flags = get_redir_flags(tok->type);
	if (tok->type == TOK_DLESS || tok->type == TOK_DLESSDASH)
	{
		redir->dest.type = REDIR_DOC;
		if (parse_heredoc(redir))
			return (1);
	}
	else
		redir->dest.type = REDIR_FILE;
	return (0);
}

/*
 ** ARGUMENTS
 ** redir: object to fill
 ** tok: represent the redirection operator
 ** tok2: represents the TOK_WORD following the redirection
 ** *err: error code, if the function returns non zero
 **
 ** returns 0 on succes, not 0 on error
 */

static int		do_parse(t_redir *redir, t_token *tok, t_token *tok2, int *err)
{
	tok->type = (tok->type == TOK_CLOBBER) ? '>' : tok->type;
	redir->type = tok->type;
	if (redir->redirected == -1)
	{
		if (tok->type == '>'
				|| tok->type == TOK_DGREAT
				|| tok->type == TOK_GREATAND)
			redir->redirected = 1;
		else
			redir->redirected = 0;
	}
	if (tok->type == TOK_LESSAND || tok->type == TOK_GREATAND)
	{
		if (tok2->val.str[0] == '-' && tok2->val.str[1] == '\0')
			redir->dest.type = REDIR_CLOSE;
		else if ((redir->dest.val.fd = parse_number(tok2->val.str)) != -1)
			redir->flags = get_redir_flags(tok->type);
		else if (!syntax_error("Bad fd number"))
			return ((*err = 1));
	}
	else if (do_parse_more(redir, tok, tok2))
		return ((*err = 1));
	return (0);
}

t_redir			*parse_redir(t_parser *parser, int *err)
{
	static int	redirs[] = {'<', '>', TOK_DLESS, TOK_DGREAT, TOK_LESSAND,
		TOK_GREATAND, TOK_LESSGREAT, TOK_DLESSDASH, TOK_CLOBBER, 0};
	t_redir		*redir;
	t_token		*tok;
	t_token		*tok2;

	if (!(redir = new_redir()))
		return (NULL);
	if ((tok = consume(parser, TOK_IO_NUMBER)))
		redir->redirected = tok->val.num;
	if ((tok = consume_ar(parser, redirs))
			&& (tok2 = consume(parser, TOK_WORD)))
	{
		if (!do_parse(redir, tok, tok2, err))
			return (redir);
	}
	else if (redir->redirected != -1 || tok)
	{
		syntax_error("unexpected token");
		*err = 2;
	}
	free(redir);
	return (NULL);
}
