/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   basic.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: agrouard <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/12/12 09:57:42 by agrouard          #+#    #+#             */
/*   Updated: 2018/12/12 11:48:02 by agrouard         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../lexer/token.h"
#include "parser.h"
#include "redir.h"

static int		do_parse_simple(t_parser *parser, t_cmd *cmd)
{
	t_token	*tok;
	t_redir	*redir;
	int		error;

	error = 0;
	while (1)
	{
		if ((tok = consume(parser, TOK_WORD)))
			ft_arrpush(cmd->val.simple->words, tok);
		else if ((redir = parse_redir(parser, &error)))
			ft_arrpush(cmd->val.simple->redirs, redir);
		else if (error)
		{
			free_cmd(cmd);
			return (1);
		}
		else
			return (0);
	}
}

t_cmd			*parse_simple(t_parser *parser, char *prompt)
{
	t_cmd	*cmd;

	if (!(cmd = new_simple()))
		return (NULL);
	while (skip_newlines(parser))
		peek_token(&parser->lexer, prompt);
	if (do_parse_simple(parser, cmd))
		return (NULL);
	if (!cmd->val.simple->words->size)
	{
		free_cmd(cmd);
		return (prompt ? syntax_error("unexpected token") : NULL);
	}
	return (cmd);
}

static t_cmd	*parse_pipes(t_parser *parser, char *prompt)
{
	t_cmd	*cmd;
	t_cmd	*right;
	t_cmd	*bin;

	if (!(cmd = parse_simple(parser, prompt)))
		return (NULL);
	if (consume(parser, '|'))
	{
		if (!(right = parse_pipes(parser, "pipe> ")))
			return (free_cmd(cmd));
		if (!(bin = new_binary(cmd, '|', right)))
		{
			free_cmd(right);
			return (free_cmd(cmd));
		}
		cmd = bin;
	}
	return (cmd);
}

t_cmd			*parse_pipeline(t_parser *parser)
{
	t_token	*bang;
	t_cmd	*pipeline;

	bang = consume(parser, '!');
	if ((pipeline = parse_pipes(parser, NULL)) && bang)
		pipeline->flags |= CMD_INVERT;
	return (pipeline);
}
