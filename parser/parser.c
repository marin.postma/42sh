/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   parser.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: agrouard <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/12/12 09:57:42 by agrouard          #+#    #+#             */
/*   Updated: 2018/12/12 11:42:25 by agrouard         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "parser.h"
#include "redir.h"
#include "../lexer/token.h"

t_cmd			*parse_list(t_parser *parser)
{
	t_cmd	*cmd;
	t_cmd	*right;
	t_cmd	*bin;
	t_token	*tok;

	if (!(cmd = parse_pipeline(parser)))
		return (NULL);
	while ((tok = consume(parser, TOK_AND_IF))
		|| (tok = consume(parser, TOK_OR_IF)))
	{
		if (!(right = parse_pipeline(parser)))
			return (free_cmd(cmd));
		if (!(bin = new_binary(cmd, tok->type, right)))
		{
			free_cmd(right);
			return (free_cmd(cmd));
		}
		cmd = bin;
	}
	return (cmd);
}

static t_cmd	*do_parse_complex(t_parser *parser, t_cmd *cmd, int type)
{
	t_token	*t;
	t_cmd	*right;
	t_cmd	*bin;

	if (!(t = peek_token(&parser->lexer, NULL))
		|| t->type == '\n')
	{
		if (!(bin = new_binary(cmd, type, NULL)))
			return (free_cmd(cmd));
	}
	else
	{
		if (!(right = parse_complex(parser)))
			return (free_cmd(cmd));
		if (!(bin = new_binary(cmd, type, right)))
		{
			free_cmd(right);
			return (free_cmd(cmd));
		}
	}
	return (bin);
}

t_cmd			*parse_complex(t_parser *parser)
{
	t_cmd	*cmd;
	t_token	*tok;

	if (!(cmd = parse_list(parser)))
		return (NULL);
	if ((tok = consume(parser, '\n')))
		return (cmd);
	if ((tok = consume(parser, ';'))
		|| (tok = consume(parser, '&')))
		cmd = do_parse_complex(parser, cmd, tok->type);
	return (cmd);
}

int				parse(t_parser *parser)
{
	t_token *t;

	while (1)
	{
		while (!(t = peek_token(&parser->lexer, NULL)) && parser->lexer.input.interrupted)
			parser->lexer.input.interrupted = 0;
		if (t)
		{
			parser->ast = parse_complex(parser);
			if (parser->lexer.input.interrupted)
			{
				continue ;
				parser->lexer.input.interrupted = 0;
			}
		}
		break ;
	}
	return (parser->ast != NULL);
}
