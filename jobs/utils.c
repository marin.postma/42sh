/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   utils.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: agrouard <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/12/12 09:52:57 by agrouard          #+#    #+#             */
/*   Updated: 2018/12/12 09:55:10 by agrouard         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "shell/shell.h"

t_proc	*new_proc(pid_t pid, t_array *words, t_array *redirs, int fg)
{
	t_proc	*p;

	if (!(p = malloc(sizeof(t_proc))))
		return (NULL);
	p->pid = pid;
	p->ret = 0;
	p->done = 0;
	p->stopped = 0;
	p->next = NULL;
	p->command = proc_str(words, redirs, fg);
	return (p);
}

void	free_proc(t_proc *p)
{
	if (!p)
		return ;
	if (p->next)
		free_proc(p->next);
	free(p->command);
	free(p);
}

t_job	*new_job(void)
{
	t_job	*j;

	if (!(j = malloc(sizeof(t_job))))
		return (NULL);
	j->proc = NULL;
	j->last = NULL;
	j->pgid = 0;
	j->index = 0;
	j->state = JOB_NONE;
	j->next = NULL;
	return (j);
}

void	free_job(t_job *job)
{
	t_job	*j;

	if ((j = g_sh->jobs) == job)
		g_sh->jobs = job->next;
	else if (j)
	{
		while (j->next)
		{
			if (j->next == job)
			{
				j->next = job->next;
				break ;
			}
			j = j->next;
		}
	}
	free_proc(job->proc);
	free(job);
}

t_job	*find_job(int index)
{
	t_job	*j;

	j = g_sh->jobs;
	while (j)
	{
		if (j->index == index)
			return (j);
		j = j->next;
	}
	return (NULL);
}
