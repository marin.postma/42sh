/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   shell.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: agrouard <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/12/12 10:40:57 by agrouard          #+#    #+#             */
/*   Updated: 2019/01/29 09:00:33 by agrouard         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "shell/shell.h"
#include "input/str.h"
#include <unistd.h>
#include <term.h>

t_sh	*g_sh = NULL;

int		init_shell(void)
{
	if (g_sh)
		return (0);
	if (!(g_sh = ft_memalloc(sizeof(t_sh))))
		return (1);
	if (setup_env() || setup_history())
		return (free_shell() - 2);
	if ((g_sh->interactive = isatty(0)))
	{
		g_sh->pgid = getpid();
		if (tgetent(NULL, get_env("TERM")) == 1
			&& !tcgetattr(STDIN_FILENO, &g_sh->orig_state))
			g_sh->interactive = 2;
		if (setpgid(g_sh->pgid, g_sh->pgid) < 0)
			g_sh->interactive = 0;
		setup_signals_shell(g_sh->interactive);
		tcsetpgrp(0, g_sh->pgid);
	}
	g_sh->run = 1;
	return (0);
}

int		free_shell(void)
{
	if (!g_sh)
		return (1);
	while (g_sh->jobs)
		free_job(g_sh->jobs);
	save_history();
	ft_arrfree(g_sh->env);
	free(g_sh);
	g_sh = NULL;
	return (0);
}

char	*get_prompt(void)
{
	t_str prompt;
	t_str temp;

	str_init(&prompt, NULL);
	str_init(&temp, "\x1b[36m> \x1b[01;32m");
	str_cat(&prompt, &temp);
	str_init(&temp, getcwd(NULL, 0));
	str_cat(&prompt, &temp);
	free(temp.buf);
	str_init(&temp, "\x1b[0m ");
	str_cat(&prompt, &temp);
	return (prompt.buf);
}
