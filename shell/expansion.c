/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   expansion.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: agrouard <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/12/12 10:40:57 by agrouard          #+#    #+#             */
/*   Updated: 2018/12/12 11:39:32 by agrouard         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "shell/shell.h"

static int		expand_home(char **res, char **str)
{
	(*str)++;
	if (!**str || **str == '/')
		return (ft_stradd(res, get_env("HOME")));
	return (0);
}

static int		expand_var(char **res, char **str)
{
	char	*var;
	size_t	len;

	(*str)++;
	len = 0;
	if (**str == '{')
		while ((*str)[len] != '}')
			len++;
	else
		while (ft_isalnum((*str)[len]) || (*str)[len] == '_')
			len++;
	if (len == 0)
		return (ft_strpush(res, '$'));
	if (!(var = ft_strsub(*str, 0, len)))
	{
		free(*res);
		return (1);
	}
	ft_stradd(res, get_env(var));
	free(var);
	*str += len - (**str != '{');
	return (0);
}

static int		escape_str(char **res, char **str, char quote)
{
	char	c;

	while ((c = *++*str))
	{
		if (c == '\\')
		{
			c = *++*str;
			if (c != '\\' && c != '$' && !is_quote(c))
				ft_strpush(res, '\\');
			ft_strpush(res, c);
			continue ;
		}
		else if (c == '$')
		{
			if (expand_var(res, str))
				return (1);
		}
		else if (c == quote)
			break ;
		else if (ft_strpush(res, c))
			return (1);
	}
	return (0);
}

char		*expand_str(char *str)
{
	char	*res;
	int		ret;

	if (!(res = ft_strnew(0)))
		return (NULL);
	if (*str == '~' && expand_home(&res, &str))
		return (NULL);
	while (*str)
	{
		ret = 0;
		if (*str == '$')
			ret = expand_var(&res, &str);
		else if (*str == '\\')
			ret = ft_strpush(&res, *++str);
		else if (is_quote(*str))
			ret = escape_str(&res, &str, *str);
		else
			ret = ft_strpush(&res, *str);
		if (ret)
			return (NULL);
		str++;
	}
	return (res);
}

int		substitute_vars(t_array *words)
{
	char	*s;
	size_t	i;
	t_token *t;

	i = 0;
	while (i < words->size)
	{
		t = words->data[i];
		if (!(s = expand_str(t->val.str)))
			return (-1);
		free(t->val.str);
		t->val.str = s;
		if (!*s)
			ft_arrremove(words, i--);
		else
			words->data[i] = s;
		i++;
	}
	return (words->size == 0);
}
