/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   path.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: agrouard <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/12/12 10:40:57 by agrouard          #+#    #+#             */
/*   Updated: 2019/01/29 09:32:16 by agrouard         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "shell/env.h"
#include <unistd.h>

char	*next_path(char *current)
{
	while (*current)
	{
		if (*current == ':')
			return (current);
		current++;
	}
	return (current);
}

char	*build_path(char *dir, size_t dlen, char *file, size_t flen)
{
	char *path;

	if (!(path = malloc(dlen + 1 + flen + 1)))
		return (NULL);
	ft_strncpy(path, dir, dlen);
	path[dlen] = '/';
	ft_strncpy(path + dlen + 1, file, flen + 1);
	return (path);
}

char	*get_fullpath(char *file)
{
	char	*path;
	char	*end;
	char	*filename;
	size_t	len;

	if (!(path = get_env("PATH")))
		return (NULL);
	len = ft_strlen(file);
	end = path;
	while (*end)
	{
		end = next_path(path);
		if (!(filename = build_path(path, end - path, file, len)))
			return (NULL);
		if (access(filename, X_OK) == 0)
			return (filename);
		free(filename);
		path = end + 1;
	}
	return (NULL);
}
