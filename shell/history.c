/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   history.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: agrouard <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/12/12 10:40:57 by agrouard          #+#    #+#             */
/*   Updated: 2019/01/29 12:35:29 by agrouard         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "shell/shell.h"
#include <unistd.h>
#include <fcntl.h>

#define HIST_MODE (S_IRUSR | S_IWUSR)

static char	*get_histfile()
{
	return (ft_strjoin(get_env("HOME"), "/."SH_NAME"_history"));
}

int			setup_history(void)
{
	int		fd;
	char	*file;
	char	*line;

	g_sh->hist_cmd = 0;
	ft_memset(g_sh->history, 0, HIST_SIZE * sizeof(char *));
	if (!(file = get_histfile()))
		return (0);
	fd = open(file, O_RDONLY);
	free(file);
	if (fd == -1)
		return (0);
	while (get_next_line(fd, &line) == 1)
		push_history(line);
	close(fd);
	return (0);
}

int			save_history(void)
{
	int		fd;
	int		i;
	char	*file;

	i = g_sh->hist_cmd;
	file = get_histfile();
	fd = open(file, O_WRONLY | O_CREAT | O_TRUNC, HIST_MODE);
	while (1)
	{
		if (g_sh->history[i])
		{
			if (fd != -1)
				ft_putendl_fd(g_sh->history[i], fd);
			free(g_sh->history[i]);
		}
		i = (i + 1) % HIST_SIZE;
		if (i == g_sh->hist_cmd)
			break;
	}
	free(file);
	if (fd != -1)
		close(fd);
	return (0);
}

int		push_history(char *cmd)
{
	free(g_sh->history[g_sh->hist_cmd]);
	g_sh->history[g_sh->hist_cmd] = cmd;
	if (++g_sh->hist_cmd == HIST_SIZE)
		g_sh->hist_cmd = 0;
	return (0);
}

char	*fetch_history(int *n, int dh)
{
	int i;

	i = *n + dh;
	if (i > 0 || i < -HIST_SIZE)
		i -= dh;
	*n = i;
	if (*n == 0)
		return (NULL);
	i += g_sh->hist_cmd;
	if (i < 0)
		i += HIST_SIZE;
	return (g_sh->history[i]);
}
