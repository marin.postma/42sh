/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   env.c                                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: agrouard <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/12/12 10:40:57 by agrouard          #+#    #+#             */
/*   Updated: 2018/12/12 11:40:53 by agrouard         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "shell/shell.h"
#include "parser/parser.h"

#include <unistd.h>

int		cmp_envar(char *var, char *name)
{
	size_t	i;

	i = 0;
	while (var[i] != '=')
	{
		if (!var[i] || !name[i])
			return (1);
		if (var[i] != name[i])
			return (1);
		i++;
	}
	return (name[i] != 0);
}

int		setup_env(void)
{
	extern char	**environ;
	char		**var;
	char		*val;
	int			shlvl;

	if (!(g_sh->env = ft_arrnew(32, free)))
		return (1);
	var = environ;
	while (*var)
	{
		if (ft_arrpush(g_sh->env, ft_strdup(*var)))
			return (1);
		var++;
	}
	if ((val = get_env("SHLVL")))
	{
		shlvl = parse_number(val);
		if ((val = ft_itoa(shlvl + 1)))
			set_env("SHLVL", val, 1);
		free(val);
	}
	return (0);
}

char	*get_env(char *name)
{
	size_t	i;

	if (!name)
		return (NULL);
	if (ft_arrfind(g_sh->env, name, cmp_envar, &i))
		return (NULL);
	return (g_sh->env->data[i] + ft_strlen(name) + 1);
}

int		set_env(char *name, char *value, int overwrite)
{
	size_t	i;
	char	*envar;

	if (!(envar = ft_strcjoin(name, '=', value)))
		return (-1);
	if (!ft_arrfind(g_sh->env, name, cmp_envar, &i))
	{
		if (!overwrite)
		{
			free(envar);
			return (2);
		}
		free(g_sh->env->data[i]);
		g_sh->env->data[i] = envar;
		return (0);
	}
	if (ft_arrpush(g_sh->env, envar))
	{
		free(envar);
		return (-1);
	}
	return (0);
}

int		unset_env(char *name)
{
	size_t	i;

	if (!name)
		return (0);
	if (ft_arrfind(g_sh->env, name, cmp_envar, &i))
		return (1);
	return (ft_arrremove(g_sh->env, i));
}
