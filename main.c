/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: agrouard <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/12/12 10:40:57 by agrouard          #+#    #+#             */
/*   Updated: 2019/01/29 12:52:04 by agrouard         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "shell/shell.h"
#include "parser/parser.h"
#include "exec/exec.h"

//BONUS: globbing
//BONUS: autocompletion
//BONUS: shell vars
//BONUS: unset and export builtins
//TODO: error message invalid envvar name
//TODO: tilde expansion
//TODO: POSIX options for env and cd (and maybe more)
//BONUS: alias
//TODO: fix error message on redirections
//BONUS: clean up job control
//BONUS: subshells
//BONUS: scripting

int		main(int argc, char **argv)
{
	int			ret;
	t_parser	parser;

	(void)argc;
	(void)argv;
	if (init_shell())
		return (0);
	ret = 0;
	while (g_sh->run)
	{
		init_parser(&parser);
		if (parse(&parser))
			ret = exec(parser.ast);
		free_parser(&parser);
	}
	free_shell();
	return (ret);
}
