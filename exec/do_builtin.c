/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   do_builtin.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: agrouard <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/12/12 09:31:03 by agrouard          #+#    #+#             */
/*   Updated: 2018/12/12 11:54:15 by agrouard         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "exec.h"
#include "parser/redir.h"

#include <fcntl.h>
#include <unistd.h>

static int	do_piping(int pin, int pout, int stdio[3])
{
	stdio[0] = -1;
	stdio[1] = -1;
	stdio[2] = -1;
	if (pin != -1)
	{
		if ((stdio[0] = dup(0)) == -1 || dup2(pin, 0) == -1)
			return (err("dup2 err stdin", 125));
		else if (pin != 0)
			close(pin);
	}
	if (pout != -1)
	{
		if ((stdio[1] = dup(1)) == -1 || dup2(pout, 1) == -1)
			return (err("dup2 err stdout", 125));
		else if (pout != 1)
			close(pout);
	}
	return (0);
}

static int	close_safe(int fd, int stdio[3])
{
	int	i;
	int	err;

	if ((i = is_stdio(fd, stdio)))
	{
		err = 0;
		if (stdio[i - 1] == -1)
		{
			if ((stdio[i - 1] = dup(fd)) == -1)
				err = 1;
			err |= close(fd);
		}
		return (err);
	}
	close(fd);
	return (0);
}

static int	redir_safe(t_redir *r, int stdio[3])
{
	int	e;

	e = 0;
	if (r->dest.type == REDIR_FILE)
		e = redir_file(r);
	else if (r->dest.type == REDIR_DOC)
		e |= redir_heredoc(r);
	else if (((fcntl(r->dest.val.fd, F_GETFL) & r->flags) != r->flags
			&& (fcntl(r->dest.val.fd, F_GETFL) & O_RDWR) != O_RDWR))
		e = err("Bad file descriptor", 125);
	if (!e && request_fd(r->redirected, stdio))
		e |= err("dup err redirection", 125);
	if (!e && dup2(r->dest.val.fd, r->redirected) == -1)
		e |= err("dup2 err redirection", 125);
	if (r->dest.type == REDIR_FILE && r->dest.val.fd != -1)
		e |= close(r->dest.val.fd);
	return (e);
}

static int	do_redirections(t_array *redirs, int stdio[3])
{
	int		e;
	size_t	i;
	t_redir	*r;

	e = 0;
	i = 0;
	while (i < redirs->size && !e)
	{
		r = redirs->data[i++];
		if (r->dest.type == REDIR_CLOSE)
			e |= close_safe(r->redirected, stdio);
		else
			e |= redir_safe(r, stdio);
	}
	return (e);
}

int			do_builtin(t_simple *simple, t_ctx *ctx, t_bltin bltin)
{
	int		stdio[3];
	int		ret;

	if (!(ret = do_piping(ctx->pin, ctx->pout, stdio)))
	{
		if (!(ret = do_redirections(simple->redirs, stdio)))
			ret = bltin(simple->words->size,
					(char **)simple->words->data);
	}
	if (restore_stdio(simple->redirs, stdio))
		return (125);
	return (ret);
}
