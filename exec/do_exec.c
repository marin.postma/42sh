/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   do_exec.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: agrouard <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/12/12 09:31:03 by agrouard          #+#    #+#             */
/*   Updated: 2018/12/12 11:53:51 by agrouard         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "exec.h"
#include "parser/redir.h"

#include <fcntl.h>
#include <unistd.h>

static int	do_piping(int pin, int pout)
{
	if (pin != -1)
	{
		if (dup2(pin, 0) == -1)
			return (err("dup2 err stdin", 125));
		else if (pin != 0)
			close(pin);
	}
	if (pout != -1)
	{
		if (dup2(pout, 1) == -1)
			return (err("dup2 err stdout", 125));
		else if (pout != 1)
			close(pout);
	}
	return (0);
}

static int	redir(t_redir *r)
{
	int	e;

	e = 0;
	if (r->dest.type == REDIR_FILE)
		e |= redir_file(r);
	else if (r->dest.type == REDIR_DOC)
		e |= redir_heredoc(r);
	else if ((fcntl(r->dest.val.fd, F_GETFL) & r->flags) != r->flags
			&& (fcntl(r->dest.val.fd, F_GETFL) & O_RDWR) != O_RDWR)
		e |= err("Bad file descriptor", 125);
	if (!e && dup2(r->dest.val.fd, r->redirected) == -1)
		e |= err("dup2 err redirection", 125);
	if (!e && r->dest.type == REDIR_FILE && r->dest.val.fd != -1)
		e |= close(r->dest.val.fd);
	return (e);
}

static int	do_redirections(t_array *redirs)
{
	int		e;
	size_t	i;
	t_redir	*r;

	e = 0;
	i = 0;
	while (i < redirs->size && !e)
	{
		r = redirs->data[i++];
		if (r->dest.type == REDIR_CLOSE)
			close(r->redirected);
		else
			e |= redir(r);
	}
	return (e);
}

static int	try_exec(char *name, t_array *words, int path)
{
	set_env("_", name, 1);
	execve(name, (char **)words->data, (char **)g_sh->env->data);
	if (access(name, F_OK) == 0)
	{
		if (is_dir(name))
			return (fail(SH_NAME, name, "Is a directory", 126));
		else if (access(name, X_OK) == -1)
			return (fail(SH_NAME, name, "Permission denied", 126));
		else
			return (fail(SH_NAME, name, "Unexpected error", 127));
	}
	else if (path)
		return (fail(SH_NAME, name, "Command not found", 127));
	else
		return (fail(SH_NAME, name, "No such file or directory", 127));
}

void		do_exec(t_array *words, t_array *redirs, t_ctx *ctx)
{
	int		ret;
	char	*name;

	close_fd_list(ctx);
	(ret = do_piping(ctx->pin, ctx->pout)) ? exit(ret) : 0;
	(ret = do_redirections(redirs)) ? exit(ret) : 0;
	name = words->data[0];
	if (ft_strchr(name, '/') != NULL)
		ret = try_exec(name, words, 0);
	else
	{
		if ((name = get_fullpath(words->data[0])))
			ret = try_exec(name, words, 1);
		else
			ret = fail(SH_NAME, words->data[0], "Command not found", 127);
		free(name);
	}
	exit(ret);
}
