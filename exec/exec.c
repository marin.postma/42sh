/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   exec.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: agrouard <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/12/12 09:31:03 by agrouard          #+#    #+#             */
/*   Updated: 2018/12/12 11:48:13 by agrouard         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "exec.h"

int		exec_cmd(t_cmd *cmd, t_job *job, t_ctx *ctx)
{
	int	ret;

	ret = 1;
	if (cmd->type == CMD_BINARY)
		ret = exec_binary(cmd, job, ctx);
	if (cmd->type == CMD_SIMPLE)
		ret = exec_simple(cmd->val.simple, job, ctx);
	return (ret);
}

int		exec(t_cmd *cmd)
{
	int		ret;
	t_ctx	ctx;
	t_job	*job;

	if (!cmd || !(job = new_job()))
		return (255);
	ctx.pin = -1;
	ctx.pout = -1;
	ctx.async = 0;
	ctx.num_fd = 0;
	ctx.to_close = NULL;
	exec_cmd(cmd, job, &ctx);
	ret = register_job(job, JOB_FG);
	return ((cmd->flags & CMD_INVERT) ? !ret : ret);
}
