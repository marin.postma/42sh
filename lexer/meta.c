/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   meta.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: agrouard <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/12/12 10:03:31 by agrouard          #+#    #+#             */
/*   Updated: 2018/12/12 10:58:34 by agrouard         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "lexer.h"
#include "libft.h"

static int	match(t_lexer *lexer, char c)
{
	if (peek_char(&lexer->input, NULL) == c)
	{
		pop_char(&lexer->input, NULL);
		return (1);
	}
	return (0);
}

static int	lex_redir(t_lexer *lexer, char c)
{
	if (c == '<')
	{
		if (match(lexer, '<'))
			return (TOK_DLESS + 5 * match(lexer, '-'));
		if (match(lexer, '&'))
			return (TOK_LESSAND);
		if (match(lexer, '>'))
			return (TOK_LESSGREAT);
		return ('<');
	}
	if (c == '>')
	{
		if (match(lexer, '>'))
			return (TOK_DGREAT);
		if (match(lexer, '&'))
			return (TOK_GREATAND);
		if (match(lexer, '|'))
			return (TOK_CLOBBER);
		return ('>');
	}
	return (0);
}

static int	lex_op(t_lexer *lexer)
{
	char	c;
	int		redir;

	c = peek_char(&lexer->input, NULL);
	if (!ft_strchr(META_CHARS, c))
		return (0);
	pop_char(&lexer->input, NULL);
	if ((redir = lex_redir(lexer, c)))
		return (redir);
	return (c);
}

int			lex_meta(t_lexer *lexer)
{
	int	type;

	if ((type = lex_op(lexer)))
	{
		add_token(lexer, type);
		return (1);
	}
	return (0);
}
