/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   lexer.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: agrouard <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/12/12 09:57:22 by agrouard          #+#    #+#             */
/*   Updated: 2019/01/29 12:47:51 by agrouard         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "lexer.h"
#include "libft.h"

static int	lex_number(t_lexer *lexer)
{
	unsigned	res;
	int		curr;
	char		c;

	res = 0;
	curr = g_in->curr;
	while ((c = peek_char(&lexer->input, NULL))
			&& ft_isdigit(c))
	{
		pop_char(&lexer->input, NULL);
		res = res * 10 + (c - '0');
		if ((long)(int)res != (long)res)
			break ;
	}
	if (c == '<' || c == '>')
	{
		add_token(lexer, TOK_IO_NUMBER);
		lexer->last->val.num = res;
		return (1);
	}
	g_in->curr = curr;
	return (0);
}

static int	lex(t_lexer *lexer, char *prompt)
{
	if (!peek_char(&lexer->input, prompt))
		return (0);
	while (peek_char(&lexer->input, NULL))
	{
		if (skip_chars(lexer))
			continue ;
		if (!(lex_meta(lexer) || lex_number(lexer) || lex_word(lexer)))
		{
			free_token(lexer->first);
			lexer->first = NULL;
			lexer->last = NULL;
			lexer->curr = NULL;
			return (0);
		}
	}
	return (1);
}

t_token		*peek_token(t_lexer *lexer, char *prompt)
{
	if (lexer->curr)
		return (lexer->curr);
	else if (lexer->first && !prompt)
		return (NULL);
	if (lex(lexer, prompt))
		return (peek_token(lexer, prompt));
	return (NULL);
}

t_token		*pop_token(t_lexer *lexer, char *prompt)
{
	t_token	*t;

	if ((t = peek_token(lexer, prompt)))
		lexer->curr = lexer->curr->next;
	return (t);
}
