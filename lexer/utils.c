/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   utils.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: agrouard <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/12/12 09:57:22 by agrouard          #+#    #+#             */
/*   Updated: 2018/12/12 11:42:02 by agrouard         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "lexer.h"
#include "libft.h"

int		add_token(t_lexer *lexer, int type)
{
	t_token *temp;

	if ((temp = ft_memalloc(sizeof(t_token))))
	{
		temp->type = type;
		if (lexer->last)
			lexer->last->next = temp;
		lexer->last = temp;
		if (lexer->first == NULL)
			lexer->first = lexer->last;
		if (lexer->curr == NULL)
			lexer->curr = lexer->last;
		return (0);
	}
	return (1);
}

void	free_token(t_token *tok)
{
	if (!tok)
		return ;
	if (tok->type == TOK_WORD)
		free(tok->val.str);
	if (tok->next)
		free_token(tok->next);
	free(tok);
}

int		is_quote(char c)
{
	return (c == '"' || c == '\'');
}

int		skip_chars(t_lexer *lexer)
{
	char c;

	while ((c = peek_char(&lexer->input, NULL)))
	{
		if (c == ' ' || c == '\t')
			pop_char(&lexer->input, NULL);
		else if (c == '#')
		{
			while (c != '\n' && c != '\0')
				c = pop_char(&lexer->input, NULL);
		}
		else
			return (0);
	}
	return (1);
}
