/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   word.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: agrouard <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/12/12 10:03:31 by agrouard          #+#    #+#             */
/*   Updated: 2018/12/12 10:58:34 by agrouard         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "lexer.h"
#include "libft.h"
#include "parser/parser.h"

static int	lex_quote(t_lexer *lexer, t_str *word, char quote)
{
	char	c;
	char	next;

	while ((c = pop_char(&lexer->input, quote == '"' ?
					"dquote> " : "quote> ")))
	{
		if (c == '\\')
		{
			next = pop_char(&lexer->input, NULL);
			str_insert(word, -1, c);
			str_insert(word, -1, next);
			continue ;
		}
		str_insert(word, -1, c);
		if (c == quote)
			break ;
	}
	if (c == '\0')
	{
		free(word->buf);
		if (!g_in->interrupted)
			syntax_error("unterminated quoted string");
		return (1);
	}
	return (0);
}

int			lex_word(t_lexer *lexer)
{
	char	c;
	t_str	word;

	str_init(&word, NULL);
	while ((c = peek_char(&lexer->input, NULL)))
	{
		if (c == '\\')
		{
			str_insert(&word, -1, pop_char(&lexer->input, NULL));
			str_insert(&word, -1, pop_char(&lexer->input, NULL));
			continue ;
		}
		if (ft_strchr(DELIM_WORD, c))
			break ;
		str_insert(&word, -1, pop_char(&lexer->input, NULL));
		if (is_quote(c))
		{
			if (lex_quote(lexer, &word, c))
				return (0);
		}
	}
	add_token(lexer, TOK_WORD);
	lexer->last->val.str = word.buf;
	return (1);
}
