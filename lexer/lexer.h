/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   lexer.h                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: agrouard <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/12/12 09:57:22 by agrouard          #+#    #+#             */
/*   Updated: 2018/12/12 10:04:11 by agrouard         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef LEXER_H
# define LEXER_H

# include "token.h"
# include "input/input.h"

typedef struct	s_lexer
{
	t_input	input;

	t_token	*first;
	t_token	*last;
	t_token	*curr;
}				t_lexer;

void			init_lexer(t_lexer *lexer);
void			free_lexer(t_lexer *lexer);

t_token			*peek_token(t_lexer *lexer, char *prompt);
t_token			*pop_token(t_lexer *lexer, char *prompt);

int				lex_meta(t_lexer *lexer);
int				lex_word(t_lexer *lexer);

int				add_token(t_lexer *lexer, int type);
void			free_token(t_token *tok);
int				is_quote(char c);
int				skip_chars(t_lexer *lexer);

#endif
