NAME = 42sh
CC = gcc
CFLAGS = -Wall -Wextra -Werror -g

LDFLAGS = -lft -ltermcap
IPATH = -I. -I./libft
LPATH = -L./libft

SRC = main.c utils.c

# Shell
SRC += $(addprefix shell/, shell.c env.c history.c expansion.c path.c signals.c)
INC += $(addprefix shell/, shell.h env.h)

# Input
SRC += $(addprefix input/, input.c mapping.c init.c str.c buf.c chars.c cursor_position.c key.c screen.c raw.c utils.c)
INC += $(addprefix input/, input.h mapping.h str.h)

# Lexer
SRC += $(addprefix lexer/, lexer.c init.c meta.c word.c utils.c)
INC += $(addprefix lexer/, lexer.h token.h)

# Parser
SRC += $(addprefix parser/, parser.c ast.c redir.c heredoc.c init.c basic.c utils.c)
INC += $(addprefix parser/, parser.h ast.h redir.h)

# Exec
SRC += $(addprefix exec/, exec.c ctx.c binary.c simple.c do_exec.c do_builtin.c redir.c)
INC += $(addprefix exec/, exec.h ctx.h)

# Builtins
SRC += $(addprefix builtins/, builtins.c exit.c cd.c env.c setenv.c echo.c jobs.c)
INC += $(addprefix builtins/, builtins.h)

# Jobs
SRC += $(addprefix jobs/, jobs.c job_wait.c job_state.c utils.c)
INC += $(addprefix jobs/, jobs.h)


OBJDIR = obj
OBJ := $(addprefix $(OBJDIR)/, $(SRC:.c=.o))

all: $(NAME)

$(NAME): $(OBJ)
	@$(MAKE) -j -C libft
	$(CC) $(OBJ) -o $(NAME) $(LPATH) $(LDFLAGS)

$(OBJDIR)/%.o: %.c $(INC)
	$(CC) $(CFLAGS) $(IPATH) -c $< -o $@

$(OBJ): | $(OBJDIR)

$(OBJDIR):
	mkdir -p $(dir $(OBJ))

clean:
	#@$(MAKE) -j -C libft clean
	rm -rf $(OBJDIR)

fclean: clean
	#@$(MAKE) -j -C libft fclean
	rm -f $(NAME)

re: fclean all
