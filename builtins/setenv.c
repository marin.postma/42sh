/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   setenv.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: agrouard <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/12/12 09:21:25 by agrouard          #+#    #+#             */
/*   Updated: 2018/12/12 09:27:19 by agrouard         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "shell/shell.h"
#include "builtins.h"

#define VAR_NAME_ALPHA "Variable name must begin with a letter"
#define VAR_NAME_ALNUM "Variable name must contain alphanumeric characters"

int	sh_setenv(int argc, char *const argv[])
{
	int i;
	int err;

	err = 1;
	if (argc == 3)
		err = ft_strequ(argv[1], "-n");
	else if (argc == 4)
		err = !ft_strequ(argv[1], "-n");
	if (err)
		return (fail("setenv", "usage", "setenv [-n] NAME VALUE", 1));
	i = 0;
	if (!ft_isalpha(argv[argc - 2][0]) || argv[argc - 2][0] == '_')
		return (fail("setenv", NULL, VAR_NAME_ALPHA, 1));
	while (argv[argc - 2][++i])
		if (!ft_isalnum(argv[argc - 2][i]) || argv[argc - 2][i] == '_')
			return (fail("setenv", NULL, VAR_NAME_ALNUM, 1));
	return (set_env(argv[argc - 2], argv[argc - 1], argv[1][0] != '-'));
}

int	sh_unsetenv(int argc, char *const argv[])
{
	if (argc != 2)
		return (fail("unsetenv", "usage", "unsetenv NAME", 1));
	return (unset_env(argv[1]));
}
