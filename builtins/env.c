/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   env.c                                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: agrouard <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/12/12 09:21:25 by agrouard          #+#    #+#             */
/*   Updated: 2018/12/12 09:27:19 by agrouard         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "shell/shell.h"
#include "builtins.h"

#define VAR_NAME_ALPHA "Variable name must begin with a letter"
#define VAR_NAME_ALNUM "Variable name must contain alphanumeric characters"

static int	parse_arg_u(char ***arg, t_array *env, int j)
{
	char	*var;
	size_t	index;

	if ((**arg)[j] != 'u')
		return (fail("env", "usage", "env [-0iu]... [-] [NAME=VALUE]...", 1));
	if ((**arg)[j + 1] == '\0')
		var = *++(*arg);
	else
		var = (**arg) + j + 1;
	if (!var)
		return (fail("env", "usage", "option requires an argument -- 'u'", 1));
	if (!ft_arrfind(env, var, cmp_envar, &index))
		ft_arrremove(env, index);
	return (0);
}

static int	parse_args(char ***arg, t_array *env, char *end)
{
	int	j;

	while (**arg && ***arg == '-')
	{
		j = 1;
		if (!(**arg)[j])
			break;
		while ((**arg)[j])
		{
			if ((**arg)[j] == '0')
				*end = '\0';
			else if ((**arg)[j] == 'i')
				ft_arrclear(env);
			else if (!parse_arg_u(arg, env, j))
				break;
			else
				return (1);
			j++;
		}
		(*arg)++;
	}
	return (0);
}

static int	dup_env(char **arg, t_array *env)
{
	size_t	k;
	size_t	index;

	k = 0;
	while (k < env->size)
	{
		env->data[k] = ft_strdup(env->data[k]);
		k++;
	}
	env->dtor = free;
	while (*arg)
	{
		if (!ft_strchr(*arg, '='))
			break;
		if (!ft_arrfind(env, *arg, cmp_envar, &index))
		{
			free(env->data[index]);
			env->data[index] = ft_strdup(*arg);
		}
		else if (ft_arrpush(env, ft_strdup(*arg)))
			return (1);
		(*arg)++;
	}
	return (0);
}

int			sh_env(int argc, char *const argv[])
{
	size_t	k;
	char	end;
	t_array	*env;
	char	**arg;

	(void)argc;
	if (!(env = ft_arrmap(g_sh->env, NULL)))
		return (-1);
	env->dtor = NULL;
	arg = (char **)argv + 1;
	end = '\n';
	if (!parse_args(&arg, env, &end))
	{
		if (dup_env(arg, env))
			return (-1);
		k = 0;
		while (k < env->size)
		{
			ft_putstr(env->data[k++]);
			ft_putchar(end);
		}
	}
	ft_arrfree(env);
	return (0);
}
