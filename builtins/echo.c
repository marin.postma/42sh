/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   echo.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: agrouard <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/12/12 09:21:25 by agrouard          #+#    #+#             */
/*   Updated: 2018/12/12 09:29:25 by agrouard         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "builtins.h"
#include <unistd.h>

static int	echo_parse_options(char *f, int *opts)
{
	static const char	*options = "ne";
	char				*index;
	int					o;

	o = 0;
	while (*f)
	{
		index = ft_strchr(options, *f);
		if (index == NULL)
			return (1);
		o |= (1 << (int)(index - options));
		f++;
	}
	*opts |= o;
	return (0);
}

static int	echo_parse_args(int argc, char *const argv[], int *options)
{
	int	i;

	i = 1;
	*options = 0;
	while (i < argc && argv[i][0] == '-')
	{
		if (echo_parse_options(argv[i++] + 1, options))
			break ;
	}
	return (i);
}

static int	escape_char(char c)
{
	if (c == '\\')
		ft_putchar('\\');
	else if (c == 'a')
		ft_putchar('\a');
	else if (c == 'b')
		ft_putchar('\b');
	else if (c == 't')
		ft_putchar('\t');
	else if (c == 'n')
		ft_putchar('\n');
	else if (c == 'v')
		ft_putchar('\v');
	else if (c == 'f')
		ft_putchar('\f');
	else if (c == 'r')
		ft_putchar('\r');
	else
		return (1);
	return (0);
}

static void	echo_print(char *str)
{
	while (*str)
	{
		if (*str != '\\')
			write(1, str++, 1);
		else if (*++str)
		{
			if (escape_char(*str++))
				write(1, str - 2, 2);
		}
	}
}

int			sh_echo(int argc, char *const argv[])
{
	int i;
	int options;

	i = echo_parse_args(argc, argv, &options);
	while (i < argc)
	{
		if (options & (1 << 1))
			echo_print(argv[i]);
		else
			ft_putstr(argv[i]);
		if (++i < argc)
			ft_putchar(' ');
	}
	if (!(options & (1 << 0)))
		ft_putchar('\n');
	return (0);
}
