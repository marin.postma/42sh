/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   exit.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: agrouard <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/12/12 09:21:25 by agrouard          #+#    #+#             */
/*   Updated: 2018/12/12 09:21:25 by agrouard         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "builtins.h"
#include "shell/shell.h"

#include <unistd.h>
#include <stdlib.h>

int	sh_exit(int argc, char *const argv[])
{
	int code;

	code = 0;
	if (argc == 2)
		code = ft_atoi(argv[1]);
	free_shell();
	exit(code);
	return (0);
}
